#
# File: DungeonGen.py
# Description: Generates a random dungeon and outputs to a text file
# Notes:
#
# Copyright (C) 2019 Dann Moore.  All rights reserved.
#

import sys
import random
import argparse
import png


# define classes
class Point:
    def __init__(self, _x, _y):
        self.x = _x
        self.y = _y


class _DungeonParameters:
   def __init__(self):
      self.width = 80
      self.height = 40
      self.dungeon_type = 0
      self.min_room_size = 5
      self.max_room_size = 10
      self.num_cells = 8
      self.min_rooms = 16
      self.max_rooms = 26
      self.num_extra_connections = 2
      self.irregular_buffer = 5
      self.irregular_min_points = 1
      self.irregular_max_points = 4
      self.caves_init_wall_pct = 58
      self.caves_num_passes = 5

class _MapCell:
   def __init__(self):
      self.room_present = False
      self.been_connected = False
      self.connected_cell_x = 0
      self.connected_cell_y = 0
      self.posx = 0
      self.posy = 0
      self.roomsizex = 0
      self.roomsizey = 0




# define a function to test whether a point is within a given polygon
def PointInPoly(pts, testx, testy):

   # Check if the test point is within pts
   for i in range(len(pts)):
      if (pts[i].x == testx) and (pts[i].y == testy): return "True"


   # Check if the test point is on a polygon edge
   for i in range(len(pts)):
      p1 = None
      p2 = None
      if i==0:
         p1 = pts[0]
         p2 = pts[1]
      else:
         p1 = pts[i-1]
         p2 = pts[i]
      if p1.y == p2.y and p1.y == testy and testx > min(p1.x, p2.x) and testx < max(p1.x, p2.x):
         return True

   n = len(pts)
   within = False

   p1x = pts[0].x
   p1y = pts[0].y
   for i in range(n + 1):
      p2x = pts[i % n].x
      p2y = pts[i % n].y
      if testy > min(p1y, p2y):
         if testy <= max(p1y, p2y):
            if testx <= max(p1x, p2x):
               if p1y != p2y:
                  xints = (testy - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
               if p1x == p2x or testx <= xints:
                  within = not within
      p1x = p2x
      p1y = p2y

   if within: return True
   else: return False











# define a function for creating rooms
def CreateRoom(x, y, width, height):
   global mapdata
   

   for i in range(width):
      for j in range(height):
         if (i + x > 0) and (i + x < dungeon_parameters.width - 1) and (j + y > 0) and (j + y < dungeon_parameters.height - 1):
            mapdata[i + x][j + y] = " "



# define a function for creating hallways
def CreateHallway(x1, y1, x2, y2):
   global mapdata

   # Choose whether to go up/down or left/right first for more randomness in hallway shapes
   verticalfirst = rng.randint(0,1)
   if verticalfirst == 1: # Go up/down first
      # Draw empty cells from the first point over to the second point, making a 90 degree turn
      if y1 > y2:
         endy = y1
         for y in range(y2, y1 + 1):
            if (x2 > 0) and (x2 < dungeon_parameters.width - 1) and (y > 0) and (y < dungeon_parameters.height - 1):
               mapdata[x2][y] = " "
      elif y1 < y2:
         endy = y2
         for y in range(y1, y2 + 1):
            if (x1 > 0) and (x1 < dungeon_parameters.width - 1) and (y > 0) and (y < dungeon_parameters.height - 1):
               mapdata[x1][y] = " "
      else:
         endy = y1

      if x1 > x2:
         for x in range(x2, x1 + 1):
            if (x > 0) and (x < dungeon_parameters.width - 1) and (endy > 0) and (endy < dungeon_parameters.height - 1):
               mapdata[x][endy] = " "

      elif x1 < x2:
         for x in range(x1, x2 + 1):
            if (x > 0) and (x < dungeon_parameters.width - 1) and (endy > 0) and (endy < dungeon_parameters.height - 1):
               mapdata[x][endy] = " "

   else: # Go left/right first
      # Draw empty cells from the first point over to the second point, making a 90 degree turn
      if x1 > x2:
         endx = x1
         for x in range(x2, x1 + 1):
            if (x > 0) and (x < dungeon_parameters.width - 1) and (y2 > 0) and (y2 < dungeon_parameters.height - 1):
               mapdata[x][y2] = " "
      elif x1 < x2:
         endx = x2
         for x in range(x1, x2 + 1):
            if (x > 0) and (x < dungeon_parameters.width - 1) and (y1 > 0) and (y1 < dungeon_parameters.height - 1):
               mapdata[x][y1] = " "
      else:
         endx = x1

      if y1 > y2:
         for y in range(y2, y1 + 1):
            if (endx > 0) and (endx < dungeon_parameters.width - 1) and (y > 0) and (y < dungeon_parameters.height - 1):
               mapdata[endx][y] = " "

      elif y1 < y2:
         for y in range(y1, y2 + 1):
            if (endx > 0) and (endx < dungeon_parameters.width - 1) and (y > 0) and (y < dungeon_parameters.height - 1):
               mapdata[endx][y] = " "
   #









# define a function to "irregularize" a room
def IrregularizeRoom(x, y, width, height):
   global dungeon_parameters
   global mapdate
 
   points = []

   # First, get the center point of the room (or close enough)	
   centerx = x + (width // 2)
   centery = y + (height // 2)

   num_points = 0


   # We are going to extend the room in all four directions, so keep track of each edge
   # top edge buffer area
   edgeleft = x
   edgetop = y - dungeon_parameters.irregular_buffer
   edgeright = x + width
   edgebottom = y

   # make sure our edges don't go out of map bounds
   if edgeleft < 1: edgeleft = 1
   if edgetop < 1: edgetop = 1
   if edgeright > dungeon_parameters.width - 2: edgeright = dungeon_parameters.width -2
   if edgebottom > dungeon_parameters.height - 2: edgebottom = dungeon_parameters.height - 2


   # Choose a random number of points in the extended buffer area
   cur_points = rng.randint(dungeon_parameters.irregular_min_points, dungeon_parameters.irregular_max_points)


   lastx = edgeleft
   for i in range(cur_points):
      # Get a random point within the buffer area
      if edgeleft > edgeright: edgeleft, edgeright = edgeright, edgeleft # Swap for randint()
      if edgetop > edgebottom: edgetop, edgebottom = edgebottom, edgetop # Swap for randint()
      p = Point(rng.randint(edgeleft, edgeright), rng.randint(edgetop, edgebottom))
      while p.x < lastx:
         p.x = rng.randint(edgeleft, edgeright)
         p.y = rng.randint(edgetop, edgebottom)

      lastx = p.x # Make sure we keep going in a clockwise direction
      num_points = num_points + 1
      points.append(p)
    



   # right edge buffer area
   edgeleft = x + width
   edgetop = y
   edgeright = x + width + dungeon_parameters.irregular_buffer
   edgebottom = y + height

   # make sure our edges don't go out of map bounds
   if edgeleft < 1: edgeleft = 1
   if edgetop < 1: edgetop = 1
   if edgeright > dungeon_parameters.width - 2: edgeright = dungeon_parameters.width -2
   if edgebottom > dungeon_parameters.height - 2: edgebottom = dungeon_parameters.height - 2


   # Choose a random number of points in the extended buffer area
   cur_points = rng.randint(dungeon_parameters.irregular_min_points, dungeon_parameters.irregular_max_points)


   lasty = edgetop
   for i in range(cur_points):
      # Get a random point within the buffer area
      if edgeleft > edgeright: edgeleft, edgeright = edgeright, edgeleft # Swap for randint()
      if edgetop > edgebottom: edgetop, edgebottom = edgebottom, edgetop # Swap for randint()
      p = Point(rng.randint(edgeleft, edgeright), rng.randint(edgetop, edgebottom))
      while p.y < lasty:
         p.x = rng.randint(edgeleft, edgeright)
         p.y = rng.randint(edgetop, edgebottom)

      lasty = p.y # Make sure we keep going in a clockwise direction
      num_points = num_points + 1
      points.append(p)





   # bottom edge buffer area
   edgeleft = x
   edgetop = y + height
   edgeright = x + width
   edgebottom = y + height + dungeon_parameters.irregular_buffer

   # make sure our edges don't go out of map bounds
   if edgeleft < 1: edgeleft = 1
   if edgetop < 1: edgetop = 1
   if edgeright > dungeon_parameters.width - 2: edgeright = dungeon_parameters.width -2
   if edgebottom > dungeon_parameters.height - 2: edgebottom = dungeon_parameters.height - 2


   # Choose a random number of points in the extended buffer area
   cur_points = rng.randint(dungeon_parameters.irregular_min_points, dungeon_parameters.irregular_max_points)


   lastx = edgeright
   for i in range(cur_points):
      # Get a random point within the buffer area
      if edgeleft > edgeright: edgeleft, edgeright = edgeright, edgeleft # Swap for randint()
      if edgetop > edgebottom: edgetop, edgebottom = edgebottom, edgetop # Swap for randint()
      p = Point(rng.randint(edgeleft, edgeright), rng.randint(edgetop, edgebottom))
      while p.x > lastx:
         p.x = rng.randint(edgeleft, edgeright)
         p.y = rng.randint(edgetop, edgebottom)

      lastx = p.x # Make sure we keep going in a clockwise direction
      num_points = num_points + 1
      points.append(p)





   # left edge buffer area
   edgeleft = x - dungeon_parameters.irregular_buffer
   edgetop = y
   edgeright = x
   edgebottom = y + height

   # make sure our edges don't go out of map bounds
   if edgeleft < 1: edgeleft = 1
   if edgetop < 1: edgetop = 1
   if edgeright > dungeon_parameters.width - 2: edgeright = dungeon_parameters.width -2
   if edgebottom > dungeon_parameters.height - 2: edgebottom = dungeon_parameters.height - 2


   # Choose a random number of points in the extended buffer area
   cur_points = rng.randint(dungeon_parameters.irregular_min_points, dungeon_parameters.irregular_max_points)


   lasty = edgebottom
   for i in range(cur_points):
      # Get a random point within the buffer area
      if edgeleft > edgeright: edgeleft, edgeright = edgeright, edgeleft # Swap for randint()
      if edgetop > edgebottom: edgetop, edgebottom = edgebottom, edgetop # Swap for randint()
      p = Point(rng.randint(edgeleft, edgeright), rng.randint(edgetop, edgebottom))
      while p.y > lasty:
         p.x = rng.randint(edgeleft, edgeright)
         p.y = rng.randint(edgetop, edgebottom)

      lasty = p.y # Make sure we keep going in a clockwise direction
      num_points = num_points + 1
      points.append(p)



   # Finally, fill the polygon we have created with these random points
   for i in range(x - dungeon_parameters.irregular_buffer, x + width + dungeon_parameters.irregular_buffer):
      for j in range(y - dungeon_parameters.irregular_buffer, y + height + dungeon_parameters.irregular_buffer):
         # To do this we test each "pixel" within range of the room + edge buffer area
         if (i > 0) and (i < dungeon_parameters.width - 1) and (j > 0) and (j < dungeon_parameters.height - 1): # Sanity check
            if PointInPoly(points, i, j):
               mapdata[i][j] = " "












# define the generation function
def GenerateDungeon():
   global dungeon_parameters
   if(dungeon_parameters.dungeon_type == 0):
      GenerateRoguelikeDungeon()
   elif(dungeon_parameters.dungeon_type == 1):
      GenerateIrregularDungeon()
   elif(dungeon_parameters.dungeon_type == 2):
      GenerateCavesDungeon()
   else:
      print("Unknown Dungeon Type!")
      quit()
   print("Generation complete!")



# define the roguelike dungeon function
def GenerateRoguelikeDungeon():
   global dungeon_parameters
   global mapdata


   mapcells = [[_MapCell() for row in range(dungeon_parameters.num_cells)] for col in range(dungeon_parameters.num_cells)]

   cellsizex = dungeon_parameters.width // dungeon_parameters.num_cells
   cellsizey = dungeon_parameters.height // dungeon_parameters.num_cells




   print("Generating Roguelike dungeon...")

   # First we divide the map into a grid of cells
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         # Initialize each cell
         mapcells[x][y].room_present = False
         mapcells[x][y].been_connected = False
         mapcells[x][y].connected_cell_x = -1
         mapcells[x][y].connected_cell_y = -1


   # Next, choose a starting cell
   startx = rng.randint(0, dungeon_parameters.num_cells - 1)
   starty = rng.randint(0, dungeon_parameters.num_cells - 1)
   mapcells[startx][starty].room_present = True

   # Next, choose the number of additional rooms to add (-1 because we have already chosen the starting cell)
   additional_rooms = rng.randint(dungeon_parameters.min_rooms, dungeon_parameters.max_rooms) - 1 

   # Next, place these additional rooms in cells
   for x in range(additional_rooms):
      cont = True
      deadlockcounter = 500 # create a counter to prevent deadlock while looping
      while cont:
         deadlockcounter = deadlockcounter - 1
         # Select a cell to place this room in
         newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
         newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
         # If this cell has no room we can use it, otherwise loop to select another cell
         if mapcells[newcellx][newcelly].room_present == False:
            mapcells[newcellx][newcelly].room_present = True
            cont = False
         elif deadlockcounter <= 0:
            cont = False

   # Now we connect the cells together
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present == True and mapcells[x][y].been_connected == False:
            # Randomly connect to another room
            deadlockcounter = dungeon_parameters.num_cells * dungeon_parameters.num_cells * 2 # to prevent deadlock while looping
            cont = True
            while cont:
               deadlockcounter = deadlockcounter - 1
               # Select a cell to connect the current cell to
               newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
               newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
               if newcellx != x or newcelly != y: # Can't connect to itself
                  if mapcells[newcellx][newcelly].room_present:
                     if mapcells[newcellx][newcelly].connected_cell_x == -1: # selected cell is not connected to anything
                        # Create a link between the cells
                        mapcells[x][y].connected_cell_x = newcellx
                        mapcells[x][y].connected_cell_y = newcelly

                        # Mark both cells as having been connected
                        mapcells[x][y].been_connected = True
                        mapcells[newcellx][newcelly].been_connected = True
                     #
                  #
               #
               if deadlockcounter <= 0 or mapcells[x][y].connected_cell_x != -1: # deadlock or cell was connected
                  cont = False
            # end while
         # 
      #
   #



   # Next we generate rooms in each cell that has one
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present:
            # Determine a random room size
            mapcells[x][y].roomsizex = rng.randint(dungeon_parameters.min_room_size, dungeon_parameters.max_room_size)
            mapcells[x][y].roomsizey = rng.randint(dungeon_parameters.min_room_size, dungeon_parameters.max_room_size)

            # Choose a starting point within the cell
            if cellsizex < mapcells[x][y].roomsizex: mapcells[x][y].posx = x * cellsizex
            else: mapcells[x][y].posx = (x * cellsizex) + rng.randint(0, cellsizex - mapcells[x][y].roomsizex)
            if cellsizey < mapcells[x][y].roomsizey: mapcells[x][y].posy = y * cellsizey
            else: mapcells[x][y].posy = (y * cellsizey) + rng.randint(0, cellsizey - mapcells[x][y].roomsizey)
				
            # Carve the room into our dungeon
            CreateRoom(mapcells[x][y].posx, mapcells[x][y].posy, mapcells[x][y].roomsizex, mapcells[x][y].roomsizey)


   # Next, connect each room together with hallways
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present:
            if mapcells[x][y].connected_cell_x != -1: # this cell has a connection
               # Choose a random point inside the current room to start from (dont use outside edge of room)
               room1x = mapcells[x][y].posx + rng.randint(1, mapcells[x][y].roomsizex - 2)
               room1y = mapcells[x][y].posy + rng.randint(1, mapcells[x][y].roomsizey - 2)

               # Choose a random point inside the connected room to end at (dont use outside edge of room)
               room2x = mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].posx + rng.randint(1, mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].roomsizex - 2)
               room2y = mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].posy + rng.randint(1, mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].roomsizey - 2)


               CreateHallway(room1x, room1y, room2x, room2y)
            #
         #
      #
   #


   # Finally, let's create a few extra connections between cells for flavor
   for i in range(dungeon_parameters.num_extra_connections):
      deadlockcounter = dungeon_parameters.num_cells * dungeon_parameters.num_cells * 2 # to prevent deadlock while looping
      cont = True
      while cont:
         deadlockcounter = deadlockcounter - 1
         newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
         newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
         if deadlockcounter <= 0 or mapcells[newcellx][newcelly].room_present: # deadlock or we found a room
            cont = False

      if deadlockcounter <= 0: break # failed to find a room
      
      # Create a hallway from this cell to another one
      room1x = mapcells[newcellx][newcelly].posx + rng.randint(1, mapcells[newcellx][newcelly].roomsizex - 2)
      room1y = mapcells[newcellx][newcelly].posy + rng.randint(1, mapcells[newcellx][newcelly].roomsizey - 2)


      # Get a random cell to connect to
      secondcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
      secondcelly = rng.randint(0, dungeon_parameters.num_cells - 1)

      if mapcells[secondcellx][secondcelly].room_present:
         # Connect to a random point within the room
         room2x = mapcells[secondcellx][secondcelly].posx + rng.randint(1, mapcells[secondcellx][secondcelly].roomsizex - 2)
         room2y = mapcells[secondcellx][secondcelly].posy + rng.randint(1, mapcells[secondcellx][secondcelly].roomsizey - 2)

         CreateHallway(room1x, room1y, room2x, room2y)

      else:
         # Connect to any point within the cell, this will give a dead-end hallway
         room2x = (secondcellx * cellsizex) + rng.randint(0, cellsizex - 1)
         room2y = (secondcelly * cellsizey) + rng.randint(0, cellsizey - 1)

         CreateHallway(room1x, room1y, room2x, room2y)














# define the irregular dungeon function
def GenerateIrregularDungeon():
   global dungeon_parameters
   global mapdata


   mapcells = [[_MapCell() for row in range(dungeon_parameters.num_cells)] for col in range(dungeon_parameters.num_cells)]

   cellsizex = dungeon_parameters.width // dungeon_parameters.num_cells
   cellsizey = dungeon_parameters.height // dungeon_parameters.num_cells



   print("Generating Irregular dungeon...")


   # First we divide the map into a grid of cells
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         # Initialize each cell
         mapcells[x][y].room_present = False
         mapcells[x][y].been_connected = False
         mapcells[x][y].connected_cell_x = -1
         mapcells[x][y].connected_cell_y = -1


   # Next, choose a starting cell
   startx = rng.randint(0, dungeon_parameters.num_cells - 1)
   starty = rng.randint(0, dungeon_parameters.num_cells - 1)
   mapcells[startx][starty].room_present = True

   # Next, choose the number of additional rooms to add (-1 because we have already chosen the starting cell)
   additional_rooms = rng.randint(dungeon_parameters.min_rooms, dungeon_parameters.max_rooms) - 1 


   # Next, place these additional rooms in cells
   for x in range(additional_rooms):
      cont = True
      deadlockcounter = 500 # create a counter to prevent deadlock while looping
      while cont:
         deadlockcounter = deadlockcounter - 1
         # Select a cell to place this room in
         newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
         newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
         # If this cell has no room we can use it, otherwise loop to select another cell
         if mapcells[newcellx][newcelly].room_present == False:
            mapcells[newcellx][newcelly].room_present = True
            cont = False
         elif deadlockcounter <= 0:
            cont = False




   # Now we connect the cells together
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present == True and mapcells[x][y].been_connected == False:
            # Randomly connect to another room
            deadlockcounter = dungeon_parameters.num_cells * dungeon_parameters.num_cells * 2 # to prevent deadlock while looping
            cont = True
            while cont:
               deadlockcounter = deadlockcounter - 1
               # Select a cell to connect the current cell to
               newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
               newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
               if newcellx != x or newcelly != y: # Can't connect to itself
                  if mapcells[newcellx][newcelly].room_present:
                     if mapcells[newcellx][newcelly].connected_cell_x == -1: # selected cell is not connected to anything
                        # Create a link between the cells
                        mapcells[x][y].connected_cell_x = newcellx
                        mapcells[x][y].connected_cell_y = newcelly

                        # Mark both cells as having been connected
                        mapcells[x][y].been_connected = True
                        mapcells[newcellx][newcelly].been_connected = True
                     #
                  #
               #
               if deadlockcounter <= 0 or mapcells[x][y].connected_cell_x != -1: # deadlock or cell was connected
                  cont = False
            # end while
         # 
      #
   #



   # Next we generate rooms in each cell that has one
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present:
            # Determine a random room size
            mapcells[x][y].roomsizex = rng.randint(dungeon_parameters.min_room_size, dungeon_parameters.max_room_size)
            mapcells[x][y].roomsizey = rng.randint(dungeon_parameters.min_room_size, dungeon_parameters.max_room_size)

            # Choose a starting point within the cell
            if cellsizex < mapcells[x][y].roomsizex: mapcells[x][y].posx = x * cellsizex
            else: mapcells[x][y].posx = (x * cellsizex) + rng.randint(0, cellsizex - mapcells[x][y].roomsizex)
            if cellsizey < mapcells[x][y].roomsizey: mapcells[x][y].posy = y * cellsizey
            else: mapcells[x][y].posy = (y * cellsizey) + rng.randint(0, cellsizey - mapcells[x][y].roomsizey)
				
            # Carve the room into our dungeon
            CreateRoom(mapcells[x][y].posx, mapcells[x][y].posy, mapcells[x][y].roomsizex, mapcells[x][y].roomsizey)


   # Next, connect each room together with hallways
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present:
            if mapcells[x][y].connected_cell_x != -1: # this cell has a connection
               # Choose a random point inside the current room to start from (dont use outside edge of room)
               room1x = mapcells[x][y].posx + rng.randint(1, mapcells[x][y].roomsizex - 2)
               room1y = mapcells[x][y].posy + rng.randint(1, mapcells[x][y].roomsizey - 2)

               # Choose a random point inside the connected room to end at (dont use outside edge of room)
               room2x = mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].posx + rng.randint(1, mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].roomsizex - 2)
               room2y = mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].posy + rng.randint(1, mapcells[mapcells[x][y].connected_cell_x][mapcells[x][y].connected_cell_y].roomsizey - 2)


               CreateHallway(room1x, room1y, room2x, room2y)
            #
         #
      #
   #


   # Next, let's create a few extra connections between cells for flavor
   for i in range(dungeon_parameters.num_extra_connections):
      deadlockcounter = dungeon_parameters.num_cells * dungeon_parameters.num_cells * 2 # to prevent deadlock while looping
      cont = True
      while cont:
         deadlockcounter = deadlockcounter - 1
         newcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
         newcelly = rng.randint(0, dungeon_parameters.num_cells - 1)
         if deadlockcounter <= 0 or mapcells[newcellx][newcelly].room_present: # deadlock or we found a room
            cont = False

      if deadlockcounter <= 0: break # failed to find a room
      
      # Create a hallway from this cell to another one
      room1x = mapcells[newcellx][newcelly].posx + rng.randint(1, mapcells[newcellx][newcelly].roomsizex - 2)
      room1y = mapcells[newcellx][newcelly].posy + rng.randint(1, mapcells[newcellx][newcelly].roomsizey - 2)


      # Get a random cell to connect to
      secondcellx = rng.randint(0, dungeon_parameters.num_cells - 1)
      secondcelly = rng.randint(0, dungeon_parameters.num_cells - 1)

      if mapcells[secondcellx][secondcelly].room_present:
         # Connect to a random point within the room
         room2x = mapcells[secondcellx][secondcelly].posx + rng.randint(1, mapcells[secondcellx][secondcelly].roomsizex - 2)
         room2y = mapcells[secondcellx][secondcelly].posy + rng.randint(1, mapcells[secondcellx][secondcelly].roomsizey - 2)

         CreateHallway(room1x, room1y, room2x, room2y)

      else:
         # Connect to any point within the cell, this will give a dead-end hallway
         room2x = (secondcellx * cellsizex) + rng.randint(0, cellsizex - 1)
         room2y = (secondcelly * cellsizey) + rng.randint(0, cellsizey - 1)

         CreateHallway(room1x, room1y, room2x, room2y)





   # Finally, let's take each room and pad the edges with an irregular pattern to give it a rough look
   for x in range(dungeon_parameters.num_cells):
      for y in range(dungeon_parameters.num_cells):
         if mapcells[x][y].room_present:
            IrregularizeRoom(mapcells[x][y].posx, mapcells[x][y].posy, mapcells[x][y].roomsizex, mapcells[x][y].roomsizey)












# define the caves dungeon function using a cellular automata method
def GenerateCavesDungeon():
   global dungeon_parameters
   global mapdata


   print("Generating Caves dungeon...")

   # First, we need to randomly fill a second map with walls and empty spaces
   new_mapdata = [["q" for row in range(dungeon_parameters.height)] for col in range(dungeon_parameters.width)]

   for x in range(dungeon_parameters.width):
      for y in range(dungeon_parameters.height):
         if rng.randint(0,100) <= dungeon_parameters.caves_init_wall_pct:
            new_mapdata[x][y] = "X"
         else:
            new_mapdata[x][y] = " "


   # Enclose the borders of the new map
   for x in range(dungeon_parameters.width): 
      new_mapdata[x][0] = "X"
      new_mapdata[x][dungeon_parameters.height - 1] = "X"


   for y in range(dungeon_parameters.height): 
      new_mapdata[0][y] = "X"
      new_mapdata[dungeon_parameters.width - 1][0] = "X"



   # Now we perform a number of passes
   for i in range(dungeon_parameters.caves_num_passes):
      # Loop through our second map
      for x in range(dungeon_parameters.width - 1):
         for y in range(dungeon_parameters.height - 1):
            # Count the number of neighboring wall tiles for each map cell
            neighbors = 0;
            if(new_mapdata[x - 1][y - 1] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x - 1][y] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x - 1][y + 1] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x][y - 1] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x][y + 1] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x + 1][y - 1] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x + 1][y] == "X"): neighbors = neighbors + 1;
            if(new_mapdata[x + 1][y + 1] == "X"): neighbors = neighbors + 1;


            # If there are 5 or more neighboring wall tiles (a majority), then set the dungeon tile to a wall also
            if neighbors >= 5:
               mapdata[x][y] = "X"
            else:
               mapdata[x][y] = " "
         #
      #	

      # If this is not the last pass, copy the generated mapdata back into new_mapdata for another pass
      if i != (dungeon_parameters.caves_num_passes - 1):
         for x in range(dungeon_parameters.width):
            for y in range(dungeon_parameters.height):
               new_mapdata[x][y] = mapdata[x][y]



   # Finally, enclose the borders of the map
   for x in range(dungeon_parameters.width): 
      mapdata[x][0] = "X"
      mapdata[x][dungeon_parameters.height - 1] = "X"


   for y in range(dungeon_parameters.height): 
      mapdata[0][y] = "X"
      mapdata[dungeon_parameters.width - 1][0] = "X"
		









def PrintDungeon():
   global mapdata

   for y in range(dungeon_parameters.height):
      for x in range(dungeon_parameters.width):
         print(mapdata[x][y], end="")



def PrintDungeonToFile(filename):
   global mapdata

   fout = open(filename, "w")
   for y in range(dungeon_parameters.height):
      for x in range(dungeon_parameters.width):
         fout.write(mapdata[x][y])
      fout.write("\n")   
   fout.close()



def PrintDungeonToImage(filename):
   global mapdata

   imagedata = [[0 for row in range(dungeon_parameters.width)] for col in range(dungeon_parameters.height)]

   for y in range(dungeon_parameters.height):
      for x in range(dungeon_parameters.width):
         if mapdata[x][y] == " ": imagedata[y][x] = 255 # flip y and x for pypng

   png.from_array(imagedata, "L").save(filename)









# Initialize variables
outfilename = "default.txt"
outimagename = "default.png"
outputtofile = False
outputtoimage = False
rng = random.SystemRandom() # initialize random number generator
dungeon_parameters = _DungeonParameters()



parser = argparse.ArgumentParser()
parser.add_argument('-dungeontype', action='store', help="Specify the type of dungeon: 1 = Rogue Like, 2 = Irregular, 3 = Caves (default:" + str(dungeon_parameters.dungeon_type) + ")", type=int, default=dungeon_parameters.dungeon_type)

parser.add_argument('-width', action='store', help="Specify the width of the dungeon (default:" + str(dungeon_parameters.width) + ")", type=int, default=dungeon_parameters.width)

parser.add_argument('-height', action='store', help="Specify the height of the dungeon (default:" + str(dungeon_parameters.height) + ")", type=int, default=dungeon_parameters.height)

parser.add_argument('-minroomsize', action='store', help="Specify the minimum room size in the dungeon. (default:" + str(dungeon_parameters.min_room_size) + ")", type=int, default=dungeon_parameters.min_room_size)

parser.add_argument('-maxroomsize', action='store', help="Specify the maximum room size in the dungeon. (default:" + str(dungeon_parameters.max_room_size) + ")", type=int, default=dungeon_parameters.max_room_size)

parser.add_argument('-minrooms', action='store', help="Specify the minimum number of rooms in the dungeon. (default:" + str(dungeon_parameters.min_rooms) + ")", type=int, default=dungeon_parameters.min_rooms)

parser.add_argument('-maxrooms', action='store', help="Specify the maximum number of rooms in the dungeon. (default:" + str(dungeon_parameters.max_rooms) + ")", type=int, default=dungeon_parameters.max_rooms)

parser.add_argument('-numcells', action='store', help="Specify the number of cells to break the map into during generation. (default:" + str(dungeon_parameters.num_cells) + ")", type=int, default=dungeon_parameters.num_cells)

parser.add_argument('-numextraconnections', action='store', help="Specify the number of extra cell connections (halls) to add during generation (default:" + str(dungeon_parameters.num_extra_connections) + ")", type=int, default=dungeon_parameters.num_extra_connections)

parser.add_argument('-irregularexpansionsize', action='store', help="Specify the expansion size for rooms when generating an irregular dungeon. (default:" + str(dungeon_parameters.irregular_buffer) + ")", type=int, default=dungeon_parameters.irregular_buffer)

parser.add_argument('-irregularminpoints', action='store', help="Specify the minimum number of points when generating an irregular dungeon (default:" + str(dungeon_parameters.irregular_min_points) + ")  More points results in more room roundness", type=int, default=dungeon_parameters.irregular_min_points)

parser.add_argument('-irregularmaxpoints', action='store', help="Specify the minimum number of points when generating an irregular dungeon (default:" + str(dungeon_parameters.irregular_max_points) + ")  More points results in more room roundness", type=int, default=dungeon_parameters.irregular_max_points)

parser.add_argument('-caveswallpct', action='store', help="Specify the percentage of walls when generating a cave dungeon (default:" + str(dungeon_parameters.caves_init_wall_pct) + ")", type=int, default=dungeon_parameters.caves_init_wall_pct)

parser.add_argument('-cavesnumpasses', action='store', help="Specify the number of passes when generating a cave dungeon (default:" + str(dungeon_parameters.caves_num_passes) + ")  More passes makes for larger and smoother caves.", type=int, default=dungeon_parameters.caves_num_passes)


parser.add_argument('-f', action='store', help="Output dungeon as text to specified filename")

parser.add_argument('-i', action='store', help="Output dungeon as a PNG to specified filename")

args = parser.parse_args()

dungeon_parameters.dungeon_type = args.dungeontype
dungeon_parameters.width = args.width
dungeon_parameters.height = args.height
dungeon_parameters.min_room_size = args.minroomsize
dungeon_parameters.max_room_size = args.maxroomsize
dungeon_parameters.num_cells = args.numcells
dungeon_parameters.min_rooms = args.minrooms
dungeon_parameters.max_rooms = args.maxrooms
dungeon_parameters.num_extra_connections = args.numextraconnections
dungeon_parameters.irregular_buffer = args.irregularexpansionsize
dungeon_parameters.irregular_min_points = args.irregularminpoints
dungeon_parameters.irregular_max_points = args.irregularmaxpoints
dungeon_parameters.caves_init_wall_pct = args.caveswallpct
dungeon_parameters.caves_num_passes = args.cavesnumpasses



if args.f is not None:
   outfilename = args.f
   outputtofile = True
else:
   outputtofile = False

if args.i is not None:
   outimagename = args.i
   outputtoimage = True
else:
   outputtoimage = False


# Do some sanity checks on inputs
if dungeon_parameters.width < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.height < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.min_room_size < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.max_room_size < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.num_cells < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.min_rooms < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.max_rooms < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.num_extra_connections < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.irregular_buffer < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.irregular_min_points < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.irregular_max_points < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.caves_init_wall_pct < 0:
   print("Negative values not allowed!")
   quit()

if dungeon_parameters.caves_num_passes < 0:
   print("Negative values not allowed!")
   quit()





if (dungeon_parameters.width // dungeon_parameters.num_cells < dungeon_parameters.min_room_size) or (dungeon_parameters.height // dungeon_parameters.num_cells < dungeon_parameters.min_room_size):
   print("Not enough space for rooms, try modifying width, height, numcells, or room size!")
   quit()

if dungeon_parameters.max_room_size < dungeon_parameters.min_room_size:
   print("Minimum room size cannot be larger than maximum room size!")
   quit()

if dungeon_parameters.max_rooms < dungeon_parameters.min_rooms:
   print("Minimum rooms cannot be larger than maximum rooms!")
   quit()

if dungeon_parameters.max_rooms > dungeon_parameters.num_cells * dungeon_parameters.num_cells:
   print("Maximum rooms cannot exceed numcells * numcells!")
   quit()

if dungeon_parameters.irregular_min_points > dungeon_parameters.irregular_max_points:
   print("Irregularminpoints cannot exceed Irregularmaxpoints!")
   quit()

if dungeon_parameters.caves_init_wall_pct > 80:
   print("Cave wall percentage may not be higher than 80%!")
   quit()









# Create a map array
mapdata = [["X" for row in range(dungeon_parameters.height)] for col in range(dungeon_parameters.width)]


GenerateDungeon()

if outputtofile:
   PrintDungeonToFile(outfilename)

if outputtoimage:
   PrintDungeonToImage(outimagename)

if outputtofile == False and outputtoimage == False:
   PrintDungeon()


