# DungeonGen

<img src="https://gitlab.com/dannmoore/dungeongen/raw/master/media/animated.gif" alt="animated" width="640"/>

### Overview:

DungeonGen is a dungeon generator similar to those used by games such as Rogue.  DungeonGen is compatible with Python 3.0 or above.

### Usage:

For a quick small dungeon with default values, run "python.exe DungeonGen.py"

Use "python.exe DungeonGen.py -h" to see all command-line options.

### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.

png.py is Copyright (C) 2006 Johann C. Rocholl et al., licensed under the MIT License, as originally found at https://github.com/drj11/pypng




